use tui::MewtrixTuiError;

use crate::tui::MewtrixTui;

mod matrix;
mod tui;

#[tokio::main]
async fn main() -> Result<(), MewtrixTuiError> {
    env_logger::init();
    log::info!("initializing mewtrix (v0.1.0)");

    let mut mewtrix_tui = MewtrixTui::new().await?;
    mewtrix_tui.auth().await?;
    mewtrix_tui.run().await
}
