mod commands;

use std::str::FromStr;

use colored::Colorize;
use rustyline::{error::ReadlineError, EditMode, Editor};
use thiserror::Error;

use crate::matrix::{
    api::auth::{AuthenticationMode, UserIdentifier},
    client::{MatrixClient, MatrixClientError},
    identifier::{MatrixId, UserId},
};

use self::commands::MewtrixCommand;

#[derive(Debug, Error)]
pub enum MewtrixTuiError {
    #[error("Failed to readline: {0}")]
    ReadlineError(#[from] ReadlineError),
    #[error("Matrix client error, exiting: {0}")]
    MatrixClientError(#[from] MatrixClientError),
}

pub struct MewtrixTui {
    pub matrix_client: MatrixClient,
    pub editor: Editor<()>,
}

impl MewtrixTui {
    pub async fn new() -> Result<Self, MewtrixTuiError> {
        log::info!("Initializing new TUI client");
        let mut editor = Editor::<()>::with_config(
            rustyline::Config::builder()
                .max_history_size(0)
                .edit_mode(EditMode::Vi)
                .auto_add_history(false)
                .tab_stop(4)
                .build(),
        );
        let raw_homeserver_url = editor.readline(&"Homeserver URL: ".bold().magenta())?;
        let homeserver = url::Url::parse(&raw_homeserver_url).expect("Invalid homeserver URL");
        if !homeserver.has_host() {
            panic!("Homeserver URL missing host");
        }

        match MatrixClient::new(homeserver).await {
            Ok(matrix_client) => Ok(Self {
                matrix_client,
                editor,
            }),
            Err(err) => {
                log::error!("Failed to initialize mewtrix: {:?}", err);
                std::process::exit(1);
            }
        }
    }

    pub async fn run(&mut self) -> Result<(), MewtrixTuiError> {
        let version_info = self.matrix_client.version().await?;
        log::info!("Homeserver supports versions: {:?}", version_info);

        loop {
            match self.readline("Mew: ".bold().magenta().to_string()) {
                Ok(line) => {
                    if line.starts_with('/') {
                        if self
                            .handle_cmd(
                                MewtrixCommand::from_str(&line[1..line.len()]).unwrap_or_default(),
                            )
                            .await?
                        {
                            break;
                        }
                    }
                }
                // Ctrl-C
                Err(MewtrixTuiError::ReadlineError(ReadlineError::Interrupted)) => {}
                // Ctrl-D
                Err(MewtrixTuiError::ReadlineError(ReadlineError::Eof)) => {
                    break;
                }
                Err(other) => return Err(other),
            }
        }

        Ok(())
    }

    pub fn readline(&mut self, input: String) -> Result<String, MewtrixTuiError> {
        Ok(self.editor.readline(input.as_str())?)
    }

    pub async fn auth(&mut self) -> Result<(), MewtrixTuiError> {
        let user_id = match UserId::from_str(
            self.readline("Username: @".bold().magenta().to_string())?
                .as_str(),
        ) {
            Ok(user_id) => user_id,
            Err(why) => {
                log::error!("Failed to parse User ID: {:?}", why);
                std::process::exit(1);
            }
        };
        let mxid = MatrixId::from(&user_id);
        log::info!("Authing as @{}", user_id);

        if !self.matrix_client.try_cache_auth(&mxid).await? {
            let password =
                rpassword::prompt_password_stdout(&"Password: ".bold().magenta()).unwrap();
            self.matrix_client
                .auth_request(
                    AuthenticationMode::Password {
                        identifier: UserIdentifier::UserMatrixId {
                            user: user_id.user.clone(),
                        },
                        password,
                    },
                    None,
                )
                .await?;
        }

        let whoami_mxid = self.matrix_client.whoami().await?;
        log::info!("Logged in as: {}", whoami_mxid);
        Ok(())
    }
}
