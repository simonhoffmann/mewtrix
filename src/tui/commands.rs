use std::str::FromStr;

use colored::Colorize;

use super::{MewtrixTui, MewtrixTuiError};

pub enum MewtrixCommand {
    Help,
    Logout,
    LogoutAll,
    Exit,
}

impl Default for MewtrixCommand {
    fn default() -> Self {
        Self::Help
    }
}

pub enum MewtrixCommandParseError {
    UnknownCommand(String),
}

impl MewtrixCommand {
    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Help => "help",
            Self::Logout => "logout",
            Self::LogoutAll => "logoutall",
            Self::Exit => "exit",
        }
    }
}

impl FromStr for MewtrixCommand {
    type Err = MewtrixCommandParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "help" => Ok(Self::Help),
            "logout" => Ok(Self::Logout),
            "logoutall" => Ok(Self::LogoutAll),
            "exit" => Ok(Self::Exit),
            _ => Err(MewtrixCommandParseError::UnknownCommand(s.to_string())),
        }
    }
}

impl MewtrixTui {
    pub async fn handle_cmd(&mut self, command: MewtrixCommand) -> Result<bool, MewtrixTuiError> {
        match command {
            MewtrixCommand::Help =>  println!("{}{}\n{}\n{}\n{}\n{}\n{}\n{}\n{} - Displays this message\n{} - Invalidates this session\n{} - Invalidates all sessions\n{} - Exits Mewtrix", "Mewtrix".magenta().bold(), "!".white().bold(), "               ".on_bright_cyan(), "               ".on_bright_magenta(), "               ".on_white(), "               ".on_bright_magenta(), "               ".on_bright_cyan(), "All commands are prefixed with a `/`".bold(), MewtrixCommand::Help.as_str().bold().magenta(), MewtrixCommand::Logout.as_str().bold().magenta(), MewtrixCommand::LogoutAll.as_str().bold().magenta(), MewtrixCommand::Exit.as_str().bold().magenta()),
            MewtrixCommand::Logout => {
                self.matrix_client.logout().await?;
                return Ok(true);
            },
            MewtrixCommand::LogoutAll => {
                self.matrix_client.logout_all().await?;
                return Ok(true);
            },
            MewtrixCommand::Exit => return Ok(true),
        }
        Ok(false)
    }
}
