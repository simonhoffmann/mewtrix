use std::{collections::HashMap, path::PathBuf};

use directories::ProjectDirs;
use serde::{Deserialize, Serialize};

lazy_static::lazy_static! {
    static ref PROJECT_DIR: ProjectDirs = ProjectDirs::from("ltd", "headpats", "mewtrix").unwrap();
}

#[derive(Debug)]
pub enum StoreError {
    FileOpenError(std::io::Error),
    ReadError(serde_json::Error),
    WriteError(serde_json::Error),
}

impl From<std::io::Error> for StoreError {
    fn from(err: std::io::Error) -> Self {
        Self::FileOpenError(err)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DeviceStore {
    pub user_id: String,
    pub access_token: String,
    pub device_id: String,
}

impl DeviceStore {
    fn path() -> PathBuf {
        let mut path = PROJECT_DIR.data_dir().to_path_buf();
        std::fs::create_dir_all(&path).expect("Failed to create project directory");
        path.push("devices.json");
        log::info!("Path: {:?}", path);
        path
    }

    pub fn read_all() -> Result<HashMap<String, Self>, StoreError> {
        serde_json::from_reader(std::fs::File::open(Self::path())?).map_err(StoreError::ReadError)
    }

    pub fn read(user_id: &str) -> Result<Option<Self>, StoreError> {
        Ok(Self::read_all()?.remove(user_id))
    }

    pub fn write_all(data: &HashMap<String, Self>) -> Result<(), StoreError> {
        serde_json::to_writer(std::fs::File::create(Self::path())?, data)
            .map_err(StoreError::WriteError)
    }

    pub fn write(data: Self) -> Result<(), StoreError> {
        let mut old_data = Self::read_all().unwrap_or_default();
        old_data.insert(data.user_id.clone(), data);
        Self::write_all(&old_data)?;
        Ok(())
    }

    pub fn delete(user_id: &str) -> Result<(), StoreError> {
        let mut old_data = Self::read_all().unwrap_or_default();
        old_data.remove(user_id);
        Self::write_all(&old_data)?;
        Ok(())
    }
}
