use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use serde_with::{serde_as, DisplayFromStr};
use std::{fmt::Display, num::ParseIntError, str::FromStr};

use regex::Regex;

use crate::matrix::client::{MatrixClient, MatrixClientError};

#[derive(Debug)]
pub struct SpecificationVersion {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
}

impl Display for SpecificationVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "r{}.{}.{}",
            self.major, self.minor, self.patch
        ))
    }
}

#[derive(Debug)]
pub enum SpecificationVersionParseError {
    InvalidVersionString,
    InvalidNumber,
}

impl Display for SpecificationVersionParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

impl From<ParseIntError> for SpecificationVersionParseError {
    fn from(_: ParseIntError) -> Self {
        Self::InvalidNumber
    }
}

impl FromStr for SpecificationVersion {
    type Err = SpecificationVersionParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static::lazy_static! {
            static ref VER_REGEX: Regex = Regex::new(r"r(\d+)\.(\d+)\.(\d+)").unwrap();
        }

        if let Some(captures) = VER_REGEX.captures(s) {
            Ok(Self {
                major: captures[1].parse()?,
                minor: captures[2].parse()?,
                patch: captures[3].parse()?,
            })
        } else {
            Err(SpecificationVersionParseError::InvalidVersionString)
        }
    }
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct VersionResponse {
    #[serde_as(as = "Vec<DisplayFromStr>")]
    pub versions: Vec<SpecificationVersion>,
    pub unstable_features: Map<String, Value>,
}

impl MatrixClient {
    pub async fn version(&self) -> Result<VersionResponse, MatrixClientError> {
        Ok(self
            .api_client
            .get::<(), _>(self.homeserver_url("/_matrix/client/versions"), None, None)
            .await?)
    }
}
